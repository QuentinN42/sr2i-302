# SR2I302 – Le Projet Securite – Jeudi 23 Fevrier 2023

Quentin Lieumont

## Introduction

Une Banque Privee de 2000 employes, souhaite offrir un acces et un service Internet et Email securises, pour l’ensemble de ses employes connectes sur le reseau de l’Entreprise, et ce de manière securisee de bout en bout.

Dans ce rapport, nous allons voir les principales menaces et risques de securite lies a ce besoin, et proposer une architecture technique qui repond le mieu au besoin, tout en s’assurant d’une securite multi-niveaux, et de bout en bout.

## Analyse des risques de sécurité

### Observation haut niveau

L'offre d'un accès et d'un service Internet et Email sécurisés pour l'ensemble des employés connectés au réseau de l'entreprise comporte certains risques de sécurité. Voici une liste de risques liés à l'ajout d'Internet et à l'Email dans une entreprise :

- Les attaques de phishing : Les attaques de phishing sont des tentatives frauduleuses d'obtenir des informations personnelles, telles que des mots de passe, des numéros de carte de crédit, ou des informations bancaires en se faisant passer pour une entreprise légitime. Il faut creer des regles de filtrage des emails.

- Les attaques de logiciels malveillants : Les logiciels malveillants, tels que les virus et les chevaux de Troie, peuvent être téléchargés par inadvertance sur le réseau de l'entreprise et causer des dommages importants aux systèmes informatiques. Il faut creer des regles de filtrage internet.

- La fuite de données : Les données sensibles peuvent être compromises si les pare-feu et autres mesures de sécurité ne sont pas suffisantes pour protéger les données des employés de l'entreprise.

- Les spams : Les spams peuvent être utilisés pour tromper les employés en leur faisant ouvrir des messages qui contiennent des liens dangereux ou des logiciels malveillants.

- Les emails internes compromis : Les emails internes compromis, tels que les emails de phishing, peuvent tromper les employés en leur faisant croire qu'ils sont envoyés par des collègues de travail légitimes.

- Les attaques par usurpation de courrier électronique : Les attaques par usurpation de courrier électronique sont des tentatives de tromper les employés en se faisant passer pour des collègues de travail ou des partenaires commerciaux légitimes.

D'un point de vu plus general, la lecture des guides de l'ANSSI peut aider a mieux comprendre les risques de securite comme par exemple le [guide de l'ANSSI sur les bases du sys admin](https://www.ssi.gouv.fr/administration/guide/securiser-ladministration-des-systemes-dinformation/).

De plus une formation devra etre mise en place pour sensibiliser les employes aux risques de securite.

### Analyse des risques lies aux mails

Fondamentallement, il ne faut pas voir l'email comme un service de messagerie mais comme un service de transfert de fichier. Il faut donc mettre en place des regles de filtrage pour les emails entrants et sortants.

Mis a part les failles specifiques au protocoles et logiciels mails (CVE) qui doivent etre evidement patch (c'est pour ca que vous payez une DSI), l'email n'est qu'une conversation entre deux acteurs.

Ainsi, il ne faut pas voir le phishing ou le spam comme des attaques specifiques a l'email mais comme des attaques generiques qui peuvent etre faites en tout lieux :

Si je vais au guichet en disant que je suis le directeur de la banque, je pourrai peut etre recuperer de l'argent. C'est ce probleme qu'il faut combatre et l'ajout de couches techniques ne semble pas fonctionner : les attaques de phishing sont en augmentation.

Pour mieux securiser le SI, la sensibilisation et l'education semblent la meilleure solution.

### Analyse des risques lies a l'internet

Internet est omnipresent dans notre vie quotidienne. Il est donc difficile de se passer d'Internet. Cependant, il faut prendre en compte les risques de securite lies a l'Internet.

De la meme maniere que pour les emails, il ne faut pas voir l'Internet comme un moyen technique mais comme des interactions entre acteurs. Certains acteurs sont malveillants et d'autres non.

Un agent peu mettre une popup sur un site web disant que l'ordinateur est infecte et il faut installer un logiciel pour le nettoyer. C'est une attaque assez rependue qui ne nescessite aucunnes competences techniques. La compexitee est de convaincre l'utilisateur de faire ce que l'ont veux.

De meme que precedement, il faut sensibiliser les employes aux risques de securite.
Et le fait de metre en place des regles obscures sans informer les employes s'est toujours solde par un echec.

De la meme maniere qu'un conducteur dois connaitre le code de la route, laisser des employes naviguer sur Internet sans les former est un risques pour l'entreprise.

## Architecture

Une fois que l'analyse precedente a ete faite, il est possible de proposer une architecture technique qui repond au besoin tout en s'assurant d'une securite multi-niveaux, et de bout en bout.

Finalement le plus simple est le mieux : Les utilisateurs se connectent a internet via une box internet.
Chaque utilisateur est isolé dans un VLAN.
Pour lui le reseau est le suivant : Son PC + Box qui le relie a Internet.

Comme cette entreprise est une banque, elle est soumise a des regles de securite et de confidentialite.
Il n'est donc pas envisageable de laisser le serveur SMTP a un sous traitant.

L'architecture proposée est la suivante :

- Un serveur de messagerie (SMTP) pour l'envoi et la reception des emails.
- Un firewall/IDS pour filtrer le trafic entrant et sortant.

Le reseau pour ces employes sera separe du reseau de production de l'entreprise (serveurs pour les services).

![Architecture](./arch.png)

Tout mouvement lateral est interdit (un VLAN par connexion).
Un utilisateur voulant consulter ses mails ou en emetre doit donc passer par le firewall/IDS qui va filtrer le trafic entrant et sortant, scanner les mails ...

On pourrai aussi ajouter un serveur de proxy pour filtrer le trafic internet et ajouter un serveur de VPN pour permettre aux employes de se connecter au reseau de l'entreprise. Ces ameliorations pourrons etre mise en place avec le retour d'experience. Ici nous commencons par une solution simple et iterons avec le temps.

Le plus important est de mettre en place une politique de securite et de sensibiliser les employes.

## Politique de securite

La politique de securite doit etre simple et claire pour les employes. Elle doit etre mise en place par l'entreprise et doit etre respectee par les employes.

Des formations peuvent etre prises pour sensibiliser les employes aux risques de securite.

D'un point de vu general, les regles de vigilence de base que chaque utilisateur d'un systeme d'information doit respecter sont de vigueur :

- Avoir un mot de passe fort.
- Disque dur chiffre.
- Ne pas le reutiliser sur d'autres sites.
- Activer la double authentification lorque c'est possible (peut etre nescessaire pour acceder a certains services).
- Ne pas partager ses identifiants.
- Avoir un antivirus (controlle par l'entreprise).
- Ne pas cliquer sur des liens suspects.
- Signaller les emails suspects a la DSI.

## Conclusion

En conclusion, l'offre d'un accès et d'un service Internet et Email sécurisés pour l'ensemble des employés connectés au réseau de l'entreprise comporte certains risques de sécurité qu'il faut prendre en compte.

Pour répondre à ce besoin de manière sécurisée, nous avons proposé une architecture technique simple mais evolutive, il faudra voir avec le temps si la demande est importante et si l'architecture doit etre mise a jour.

Enfin, une politique de sécurité simple et claire pour les employés doit être mise en place par l'entreprise et inclure des règles de vigilance de base. Des formations pour sensibiliser les employés aux risques de sécurité sont également recommandées.
